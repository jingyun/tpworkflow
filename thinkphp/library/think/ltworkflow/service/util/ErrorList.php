<?php
namespace think\ltworkflow\service\util;

/**
 * 错误记录
 */
class ErrorList{
	private static $errorList = array();
	/**
	 * 
	 * 添加错误记录
	 */
	public static function add($errorBox) {
		if(get_class($errorBox) == 'ErrorBox'){
			array_push(self::$errorList, $errorBox);
			return true;
		}else{
			return false;
		}
	}
	/**
	 * 
	 * 取出错误记录
	 */
	public static function getList() {
		return self::$errorList;
	}
	/**
	 * 
	 * 是否有错误信息
	 */
	public static function haveError() {
		if(empty(self::$errorList)){
			return false;
		}else{
			return true;
		} 
	}
	
	
}

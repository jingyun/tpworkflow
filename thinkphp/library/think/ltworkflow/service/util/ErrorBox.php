<?php
namespace think\ltworkflow\service\util;
/**
 * 
 * 错误类
 * 错误编码$code
 * 0  未定义错误类型
 * 1  sql语句执行错误
 * 2  工作流检查错误
 * 3  自定义脚本校验错误
 *
 */
class ErrorBox{
	var $code = "";//错误编码
	var $message = "";//错误描述
	
	public function __construct($message,$code){
		$this->message = $message;
		$this->code = $code;
		
	}
}
